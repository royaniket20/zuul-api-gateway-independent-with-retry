There are many cache layers and intervals between Eureka Client and Eureka Server. So your Eureka client cannot recognize the change of status instantly. But you can reduce the delay with the following properties.


eureka.server.responseCacheUpdateInvervalMs

define in eureka server.
It's eureka server's response cache time of API. Default is 30 seconds and it's too big for most cases. You can reduce this value.


eureka.client.registryFetchIntervalSeconds


define in eureka client.
interval to fetch registry from Eureka Server. You can reduce it.

ribbon.ServerListRefreshInterval


define in ribbon(eureka) client.
Usually Eureka is being used with Ribbon. Therefore, Ribbon's fetching interval is also important to reduce the delay. Default is 30s. You can reduce it.