package com.aniket.ServiceProvider;

import lombok.Data;

@Data
public class Response {
	
	private String name;
	private int port;

}
