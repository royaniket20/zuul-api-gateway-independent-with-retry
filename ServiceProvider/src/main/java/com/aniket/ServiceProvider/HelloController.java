package com.aniket.ServiceProvider;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

	@Autowired
	private ServerProperties serverProperties;
	
	@GetMapping("/hello")
	public Response hello() {
		Response response = new Response();
		response.setName("Service Provider Running");
		response.setPort(serverProperties.getPort());
		return response;
	}
	
}
